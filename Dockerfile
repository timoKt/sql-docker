FROM mysql:latest

ENV MYSQL_ROOT_PASSWORD=root

COPY ./sql_database_tutorial.sql /docker-entrypoint-initdb.d/
