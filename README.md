### Install
1. Install docker
2. Build a docker image
docker build -t [tag] [dockerfile position]
for our example execute: 
```
docker build -t mysql_db .
```

3. Check if new image was created
```
docker images

```
4. Create and or run Docker container
docker run [docker image]
for our example execute: 
```
docker run mysql_db
```

5. Get CONTAINER_ID of docker container
```
docker container ls
```

6. How to get into a docker container
```
docker exec -it [CONTAINER_ID] /bin/bash
```

6. Start mysql
(password is root)
```
mysql -proot 
```

7. Type basic mysql commands
```
use student;
```
```
show tables;
```
```
SELECT * FROM students;
```
